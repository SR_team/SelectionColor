#pragma once

#include "SelectionColor_global.h"
#include "Qt-Color-Widgets/include/QtColorWidgets/color_wheel.hpp"
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "DeviceView.h"

class SELECTIONCOLOR_EXPORT SelectionColor : public QWidget {
	Q_OBJECT

	QGridLayout *gridLayout;
	QVBoxLayout *verticalLayout;
	DeviceView *device;
	color_widgets::ColorWheel *color;

	void setupUi( QWidget *SelectionColor ) {
		if ( SelectionColor->objectName().isEmpty() ) SelectionColor->setObjectName( QString::fromUtf8( "SelectionColor" ) );
		SelectionColor->resize( 774, 565 );
		gridLayout = new QGridLayout( SelectionColor );
		gridLayout->setObjectName( QString::fromUtf8( "gridLayout" ) );
		verticalLayout = new QVBoxLayout();
		verticalLayout->setObjectName( QString::fromUtf8( "verticalLayout" ) );
		device = new DeviceView( SelectionColor );
		device->setObjectName( QString::fromUtf8( "device" ) );

		verticalLayout->addWidget( device );

		color = new color_widgets::ColorWheel( SelectionColor );
		color->setObjectName( QString::fromUtf8( "color" ) );
		color->setMaximumSize( QSize( 16777215, 200 ) );

		verticalLayout->addWidget( color );


		gridLayout->addLayout( verticalLayout, 0, 0, 1, 1 );


		retranslateUi( SelectionColor );

		QMetaObject::connectSlotsByName( SelectionColor );
	} // setupUi

	void retranslateUi( QWidget *SelectionColor ) {
		SelectionColor->setWindowTitle( QCoreApplication::translate( "SelectionColor", "Form", nullptr ) );
	} // retranslateUi

public:
	[[maybe_unused]] explicit SelectionColor( QWidget *parent = nullptr );

	void setController( class RGBController *controller_ptr );

	const QVector<int> &getSelectedLeds() const;
	void selectLeds( const QVector<int> &leds );

	void setColor( const QColor &color );
	void setColorRand();
	QColor getColor() const;

protected slots:
	void onSelectionChanged( QVector<int> );
	void onColorSelected( QColor );

signals:
	// Emited, when user change selection or collor
	void selected( const QVector<int> &, const QColor & );
	// Emited on all changes. Include user actions and methods like setColor
	void changed( const QVector<int> &, const QColor & );
};

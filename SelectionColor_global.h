#pragma once

#include <QtCore/qglobal.h>

#if defined(SELECTIONCOLOR_LIBRARY)
#  define SELECTIONCOLOR_EXPORT Q_DECL_EXPORT
#else
#  define SELECTIONCOLOR_EXPORT Q_DECL_IMPORT
#endif

#include "SelectionColor.h"
#include <QRandomGenerator>

SelectionColor::SelectionColor( QWidget *parent ) : QWidget( parent ) {
	setupUi( this );
	connect( device, &DeviceView::selectionChanged, this, &SelectionColor::onSelectionChanged );
	connect( color, &color_widgets::ColorWheel::colorSelected, this, &SelectionColor::onColorSelected );
}

void SelectionColor::setController( RGBController *controller_ptr ) {
	device->setController( controller_ptr );
	if ( getSelectedLeds().empty() ) device->selectAllZones();
	emit changed( getSelectedLeds(), getColor() );
}

const QVector<int> &SelectionColor::getSelectedLeds() const {
	return device->getSelectedLeds();
}

void SelectionColor::selectLeds( const QVector<int> &leds ) {
	device->blockSignals( true );
	device->selectLeds( leds );
	device->blockSignals( false );
	emit changed( getSelectedLeds(), getColor() );
}

void SelectionColor::setColor( const QColor &color ) {
	this->color->setColor( color );
	emit changed( getSelectedLeds(), color );
}

void SelectionColor::setColorRand() {
	auto r = QRandomGenerator::global()->bounded( 255 );
	auto g = QRandomGenerator::global()->bounded( 255 );
	auto b = QRandomGenerator::global()->bounded( 255 );
	setColor( { r, g, b } );
}

QColor SelectionColor::getColor() const {
	return color->color();
}

void SelectionColor::onSelectionChanged( QVector<int> select ) {
	if ( select.empty() ) device->selectAllZones();
	emit selected( getSelectedLeds(), getColor() );
	emit changed( getSelectedLeds(), getColor() );
}

void SelectionColor::onColorSelected( QColor color ) {
	emit selected( getSelectedLeds(), color );
	emit changed( getSelectedLeds(), color );
}
